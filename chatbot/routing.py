from channels.routing import ProtocolTypeRouter, URLRouter
import agent.routing

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': URLRouter(
        agent.routing.websocket_urlpatterns
        ),
})