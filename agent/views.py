from django.shortcuts import render,HttpResponse
from .models import *
import uuid 
from django.core.cache import cache
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse
from django.db.models import Q, F
# Create your views here.
def random_code():
    code = uuid.uuid4().hex[:6].upper()
    return code

def index(request):
    data = {}
    data['random_code'] = random_code()
    return render(request,'index.html',data)

def get_command(request):
	m = request.GET['message']
	try:
		m = int(m)
	except ValueError:
		m = m
	if isinstance(m, int) is True:
		count_data = QA.objects.filter(id=int(m)).count()
		if count_data == 0 :
			answer = "Jawaban tidak ditemukan"
		else:
			answer = QA.objects.filter(id=m).values('answer')[0]['answer']
	else:
		count_data = QA.objects.filter(question__contains=m).count()
		if count_data == 0 :
			answer = "Jawaban tidak ditemukan"
		else:
			data = QA.objects.filter(question__contains=m).count()
			if data > 1:
				data = QA.objects.filter(question__contains=m)
				answer = ''.join([str(d.id) +". "+str(d.question)+" \n" for d in data]) + ' \n Silahkan input nomor pertanyaan'
			else:
				answer = QA.objects.filter(question__contains=m).values('answer')[0]['answer']
			answer = answer
	return HttpResponse(answer)

