from django.db import models
from django.db.models.deletion import DO_NOTHING

# Create your models here.
class QA(models.Model):
	question = models.CharField(max_length=255,null=True)
	answer = models.TextField(null=True)	
	def __str__(self):
		return self.question

class ask_user(models.Model):
	random_code = models.CharField(max_length=100,null=True,blank=True)
	name = models.CharField(max_length=100,null=True,blank=True)
	email = models.CharField(max_length=100,null=True,blank=True)
	datetime = models.DateTimeField(auto_now_add=True,null=True)
	def __str__(self):
		return self.name

class chat_history(models.Model):
	user_question = models.CharField(max_length=255,null=True)
	answer = models.TextField(null=True)
	datetime = models.DateTimeField(auto_now_add=True)
	ask_user_id = models.ForeignKey(ask_user,on_delete=DO_NOTHING,null=True)
	def __str__(self):
		return self.question