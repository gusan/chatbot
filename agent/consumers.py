import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from . import tasks
from .views import *

COMMANDS = {
    'help': {
        'help': 'Display help message.',
    },
    'sum': {
        'args': 2,
        'help': 'Calculate sum of two integer arguments. Example: `sum 12 32`.',
        'task': 'add'
    },
    'status': {
        'args': 1,
        'help': 'Check website status. Example: `status twitter.com`.',
        'task': 'url_status'
    },
}
def get_command(m):	
    try:
        m = int(m)
    except ValueError:
        m = m
    if isinstance(m, int) is True:
        count_data = QA.objects.filter(id=int(m)).count()
        if count_data == 0:
            variabel = "Jawaban tidak ditemukan. <br/> Silahakan input nomor pertanyaan dibawah?<br/>"
            data = QA.objects.all()
            answer = variabel + ' <br/>'.join([str(d.id) +". "+str(d.question)+" <br/>" for d in data]) 
        else:
            answer = QA.objects.filter(id=m).values('answer')[0]['answer']
    else:
        count_data = QA.objects.filter(question__contains=m).count()
        if count_data == 0:
            variabel = "Jawaban tidak ditemukan.<br/> Silahakan input nomor pertanyaan dibawah?<br/>"
            data = QA.objects.all()
            answer = variabel + ' \n'.join([str(d.id) +". "+str(d.question)+" <br/>" for d in data]) 
        else:
            data = QA.objects.filter(question__contains=m).count()
            if data > 1:
                variabel =  'Apakah pertanyaan anda seperti dibawah? Jika benar, input nomor pertanyaan <br/>' 
                data = QA.objects.filter(question__contains=m)
                answer = variabel+''.join([str(d.id) +". "+str(d.question)+" <br/>" for d in data]) 
            else:
                answer = QA.objects.filter(question__contains=m).values('answer')[0]['answer']
            answer = answer
    return answer

class ChatConsumer(WebsocketConsumer):
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = str(text_data_json['message'])
        response_message = 'Please type `bantuan` for the list of the commands.'
        message_parts = message.split()
        if message_parts:
            command = message_parts[0].lower()
            response_message = get_command(command)
        
        async_to_sync(self.channel_layer.send)(
                self.channel_name,
                {
                    'type': 'chat_message',
                    'message': response_message
                }
            )

    def chat_message(self, event):
        message = event['message']
        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': f' { message}'
        }))
